#!/bin/bash

set -e

export ACCEPT_LICENSE=Google-TOS
export BOARD=trogdor

cmd_setup() {
    setup_board --board=${BOARD}
}

cmd_checkout() {
    echo "board: ${BOARD}"
    cros_workon --board ${BOARD} start depthcharge
    cd ../platform/depthcharge
    git fetch \
        https://gitlab.collabora.com/chromium/depthcharge.git \
        firmware-trogdor-13577.B-collabora
    git checkout FETCH_HEAD
    cd -
}

cmd_build() {
    emerge-${BOARD} chromeos-bootimage
    cp /build/${BOARD}/firmware/image-${BOARD}.dev.bin firmware/trogdor-new.bin
}

cmd_$1

exit 0
