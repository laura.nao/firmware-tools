#!/bin/bash

set -e

export ACCEPT_LICENSE=Google-TOS
export BOARD=hana

cmd_setup() {
    ./setup_board --board=$BOARD
}

cmd_checkout() {
    echo "board: ${BOARD}"
    cros_workon --board ${BOARD} start depthcharge
    cd ../platform/depthcharge
    git fetch \
        https://gitlab.collabora.com/chromium/depthcharge.git \
        firmware-oak-8438.B-collabora
    git checkout FETCH_HEAD
    cd -
}

cmd_build() {
    emerge-${BOARD} chromeos-bootimage
    cp /build/hana/firmware/image.dev.bin ../../hana.bin
}

cmd_$1

exit 0
