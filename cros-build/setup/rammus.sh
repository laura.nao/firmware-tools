#!/bin/bash

set -e

export ACCEPT_LICENSE=Google-TOS
export BOARD=rammus

cmd_setup() {
    ./setup_board --board=$BOARD
}

cmd_checkout() {
    echo "board: ${BOARD}"
    cros_workon --board ${BOARD} start depthcharge
    cd ../platform/depthcharge
    git fetch \
        https://gitlab.collabora.com/chromium/depthcharge.git \
        firmware-rammus-11275.B-collabora
    git checkout FETCH_HEAD
    cd -
}

cmd_build() {
    patch -d/ --forward -p0 < rammus_files/0001-depthcharge-999.ebuild.patch
    emerge-${BOARD} depthcharge
}

cmd_image() {
    local output=firmware/${BOARD}-new.bin

    cp firmware/${BOARD}.bin ${output}
    cbfstool \
        ${output} \
        remove \
        -n fallback/payload \
        -r COREBOOT
    cbfstool \
        ${output} \
        add-payload \
        -r COREBOOT \
        -n fallback/payload \
        -f /build/rammus/firmware/depthcharge/dev.elf

    ls -l ${output}
}

cmd_$1

exit 0
