#!/bin/bash

set -e

env

echo "device: $CROS_DEVICE"
chroot_dir=$HOME/chroot-"$CROS_DEVICE"
cache_dir=$HOME/cache

if [ "$CROS_DEVICE" = "volteer" ]; then
    cp /home/cros-build/chromiumos/src/scripts/${CROS_DEVICE}_files/.gitcookies /home/cros-build
    src/config/setup_project.sh volteer volteer ${CROS_SDK_BRANCH}
    repo sync -j4
    repo start def --all
fi

echo "Creating CrOS SDK chroot..."
cros_sdk \
    --enter \
    --nouse-image \
    --no-ns-pid \
    --debug \
    --chroot "$chroot_dir" \
    --cache-dir "$cache_dir" \
    $@

exit 0
